#pragma once

#include <code_sequence.hpp>

TEST_CASE("empty code sequence") {
    CHECK_THROWS_AS(CodeSequence{{}}, std::runtime_error);
}

TEST_CASE("negative code numbers") {
    const std::vector<std::vector<CodeNumber>> invalid = {
        {0},
        {-1},
        {0, 1},
        {1, 2, 3, 0},
        {-1, -2, -3, 4},
    };

    for (const auto& code_numbers : invalid) {
        CHECK_THROWS_AS(CodeSequence{code_numbers}, std::runtime_error);
    }
}

TEST_CASE("illegal code sequences") {

    const std::vector<std::vector<CodeNumber>> illegal = {
        {1}, // 1
        {2}, // 0

        {3, 5}, // 11
        {1, 2}, // 10
        {4, 7}, // 01

        {1, 3, 8},    // 110
        {15, 4, 7},   // 101
        {32, 17, 81}, // 011
        {3, 12, 18},  // 100
        {8, 21, 78},  // 010
        {38, 52, 25}, // 001
        {2, 4, 8},    // 000

        {15, 37, 55, 21}, // 1111
        {15, 37, 55, 20}, // 1110
        {15, 37, 54, 21}, // 1101
        {15, 38, 55, 21}, // 1011
        {16, 37, 55, 21}, // 0111
    };

    for (const auto& code_numbers : illegal) {
        CHECK_THROWS_AS(CodeSequence{code_numbers}, std::runtime_error);
    }
}

TEST_CASE("repeaters") {

    // TODO add more cases
    const std::vector<std::pair<std::vector<CodeNumber>, std::vector<CodeNumber>>> repeaters = {
        {{1, 1, 1, 1, 1, 1}, {1, 1, 1}},
        {{1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1}},
        {{1, 1, 4, 1, 1, 4}, {1, 1, 4, 1, 1, 4}},
    };

    for (const auto& p : repeaters) {
        const auto code_seq1 = CodeSequence{p.first};
        const auto code_seq2 = CodeSequence{p.second};

        CHECK(code_seq1 == code_seq2);
    }
}

TEST_CASE("order") {
    const std::vector<std::pair<std::vector<CodeNumber>, std::vector<CodeNumber>>> codes = {

        {{1, 1, 3}, {1, 1, 3}},
        {{3, 1, 1}, {1, 1, 3}},
        {{1, 3, 1}, {1, 1, 3}},
        {{2, 4}, {2, 4}},
        {{4, 2}, {2, 4}},
    };

    for (const auto& p : codes) {
        const auto code_seq1 = CodeSequence{p.first};
        const auto code_seq2 = CodeSequence{p.second};

        CHECK(code_seq1 == code_seq2);
    }
}

TEST_CASE("type") {
    const std::vector<std::pair<std::vector<CodeNumber>, CodeType>> types = {
        {{1, 1, 1}, CodeType::SON},
        {{2, 2}, CodeType::UEP},
        {{1, 1, 2, 1, 3, 2}, CodeType::UEN},
        {{1, 1, 1, 1, 2, 1, 1, 1, 1, 2}, CodeType::SEP},
        {{1, 1, 2, 2, 1, 1, 3, 3}, CodeType::SEN}};

    for (const auto& p : types) {
        const auto code_seq = CodeSequence{p.first};
        const auto code_type = code_seq.type();

        CHECK(code_type == p.second);
    }
}

TEST_CASE("angles") {

    const std::vector<std::pair<std::vector<CodeNumber>, std::vector<XYZ>>> input = {
        // OSO
        {{1, 1, 1}, {XYZ::X, XYZ::Y, XYZ::Z}},
        // CNS
        {{2, 2}, {XYZ::X, XYZ::Y}},
        // OSO
        {{1, 1, 3}, {XYZ::X, XYZ::Y, XYZ::Z}},
        // CNS
        {{1, 2, 1, 2}, {XYZ::X, XYZ::Y, XYZ::X, XYZ::Z}},
        // CNS
        {{1, 1, 2, 1, 1, 2}, {XYZ::X, XYZ::Y, XYZ::Z, XYZ::Y, XYZ::X, XYZ::Z}},
        // ONS
        {{1, 1, 2, 1, 3, 2}, {XYZ::X, XYZ::Y, XYZ::Z, XYZ::Y, XYZ::X, XYZ::Z}},
        // CS
        {{1, 1, 1, 1, 2, 1, 1, 1, 1, 2}, {XYZ::X, XYZ::Y, XYZ::Z, XYZ::X, XYZ::Y, XYZ::X, XYZ::Z, XYZ::Y, XYZ::X, XYZ::Z}},
        // OSNO
        {{1, 1, 2, 2, 1, 1, 3, 3}, {XYZ::X, XYZ::Y, XYZ::Z, XYZ::Y, XYZ::Z, XYZ::X, XYZ::Y, XYZ::Z}},
    };

    for (const auto& p : input) {

        const auto code_sequence = CodeSequence{p.first};
        const auto code_angles = code_sequence.angles(InitialAngles{XYZ::X, XYZ::Y});

        CHECK(code_angles == p.second);
    }
}

TEST_CASE("constraint") {

    const std::vector<std::pair<std::vector<CodeNumber>, std::string>> input = {
        {{1, 1, 1}, "0"},
        {{2, 2}, "x-y"},
        {{1, 1, 3}, "0"},
        {{1, 2, 1, 2}, "x-eta"},
        {{1, 1, 2, 1, 1, 2}, "x-y"},
        {{1, 1, 2, 1, 3, 2}, "2x-y"},
        {{1, 1, 1, 1, 2, 1, 1, 1, 1, 2}, "0"},
        {{1, 1, 2, 2, 1, 1, 3, 3}, "0"},
    };

    for (const auto& p : input) {

        const auto code_sequence = CodeSequence{p.first};

        const auto constraint = code_sequence.constraint(InitialAngles{XYZ::X, XYZ::Y});

        const auto actual_constraint = parse_lin_com_arr_xyeta(p.second);

        CHECK(constraint == actual_constraint);
    }
}
