package billiards.codeseq;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.api.list.primitive.IntList;
import org.eclipse.collections.api.tuple.Pair;
import org.eclipse.collections.impl.list.mutable.FastList;
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;
import org.eclipse.collections.impl.tuple.Tuples;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public final class CodeSequenceTest {

    @Test
    public static void testEmptyCodeSequence() {
        Assertions.assertThrows(RuntimeException.class, () -> new CodeSequence(IntArrayList.newListWith()));
    }

    @Test
    public static void testNegativeCodeNumbers() {
        final MutableList<IntList> invalidCodeNumbers = FastList.newListWith(
            IntArrayList.newListWith(0),
            IntArrayList.newListWith(-1),
            IntArrayList.newListWith(0, 1),
            IntArrayList.newListWith(1, 2, 3, 0),
            IntArrayList.newListWith(-1, 2, -3, 4));

        for (final IntList codeNumbers : invalidCodeNumbers) {
            Assertions.assertThrows(RuntimeException.class, () -> new CodeSequence(codeNumbers));
        }
    }

    @Test
    public static void testIllegalCodeSequences() {
        final MutableList<IntList> illegalCodeSequences = FastList.newList();

        illegalCodeSequences.add(IntArrayList.newListWith(1)); // O
        illegalCodeSequences.add(IntArrayList.newListWith(2)); // E

        illegalCodeSequences.add(IntArrayList.newListWith(3, 5)); // OO
        illegalCodeSequences.add(IntArrayList.newListWith(1, 2)); // OE
        illegalCodeSequences.add(IntArrayList.newListWith(4, 7)); // EO

        illegalCodeSequences.add(IntArrayList.newListWith(1, 3, 8));    // OOE
        illegalCodeSequences.add(IntArrayList.newListWith(15, 4, 7));   // OEO
        illegalCodeSequences.add(IntArrayList.newListWith(32, 17, 81)); // EOO
        illegalCodeSequences.add(IntArrayList.newListWith(3, 12, 18));  // OEE
        illegalCodeSequences.add(IntArrayList.newListWith(8, 21, 78));  // EOE
        illegalCodeSequences.add(IntArrayList.newListWith(38, 52, 25)); // EEO
        illegalCodeSequences.add(IntArrayList.newListWith(2, 4, 8));    // EEE

        illegalCodeSequences.add(IntArrayList.newListWith(15, 37, 55, 21)); // OOOO
        illegalCodeSequences.add(IntArrayList.newListWith(15, 37, 55, 20)); // OOOE
        illegalCodeSequences.add(IntArrayList.newListWith(15, 37, 54, 21)); // OOEO
        illegalCodeSequences.add(IntArrayList.newListWith(15, 38, 55, 21)); // OEOO
        illegalCodeSequences.add(IntArrayList.newListWith(16, 37, 55, 21)); // EOOO

        for (final IntList codeNumbers : illegalCodeSequences) {
            Assertions.assertThrows(RuntimeException.class, () -> new CodeSequence(codeNumbers));
        }
    }

    @Test
    public static void testRepeaters() {
        final MutableList<Pair<IntList, IntList>> repeaters = FastList.newListWith(
                Tuples.pair(IntArrayList.newListWith(1, 1, 1, 1, 1, 1), IntArrayList.newListWith(1, 1, 1)),
                Tuples.pair(IntArrayList.newListWith(1, 1, 1, 1, 1, 1, 1, 1, 1), IntArrayList.newListWith(1, 1, 1)),
                Tuples.pair(IntArrayList.newListWith(1, 1, 4, 1, 1, 4), IntArrayList.newListWith(1, 1, 4, 1, 1, 4)));

        for (final Pair<IntList, IntList> pair : repeaters) {
            final CodeSequence codeSeq = new CodeSequence(pair.getOne());
            Assertions.assertEquals(codeSeq.codeNumbers, pair.getTwo());
        }
    }

    @Test
    public static void testOrder() {
        final MutableList<Pair<IntList, IntList>> codes = FastList.newListWith(
            Tuples.pair(IntArrayList.newListWith(1, 1, 3), IntArrayList.newListWith(1, 1, 3)),
            Tuples.pair(IntArrayList.newListWith(3, 1, 1), IntArrayList.newListWith(1, 1, 3)),
            Tuples.pair(IntArrayList.newListWith(2, 4), IntArrayList.newListWith(2, 4)),
            Tuples.pair(IntArrayList.newListWith(4, 2), IntArrayList.newListWith(2, 4)));

        for (final Pair<IntList, IntList> pair : codes) {
            final CodeSequence codeSeq = new CodeSequence(pair.getOne());
            Assertions.assertEquals(codeSeq.codeNumbers, pair.getTwo());
        }
    }

    @Test
    public static void testCodeType() {
        final MutableList<Pair<IntList, CodeType>> classifications = FastList.newListWith(
            Tuples.pair(IntArrayList.newListWith(1, 1, 1), CodeType.OSO),
            Tuples.pair(IntArrayList.newListWith(2, 2), CodeType.CNS),
            Tuples.pair(IntArrayList.newListWith(1, 1, 2, 1, 3, 2), CodeType.ONS),
            Tuples.pair(IntArrayList.newListWith(1, 1, 1, 1, 2, 1, 1, 1, 1, 2), CodeType.CS),
            Tuples.pair(IntArrayList.newListWith(1, 1, 2, 2, 1, 1, 3, 3), CodeType.OSNO));

        for (final Pair<IntList, CodeType> pair : classifications) {
            final CodeSequence codeSeq = new CodeSequence(pair.getOne());
            Assertions.assertEquals(codeSeq.type(), pair.getTwo());
        }
    }
}
