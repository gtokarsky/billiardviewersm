This project has two components: the C++ backend that analyzes and verifies the cover, and a Java GUI that allows inspecting the cover visually.

This code was written, developed, and tested on Linux and Mac. The C++ backend should compile on any Linux distribution. The Java GUI is written in the JavaFX framework, which should work on any distribution that packages Java 11.

Note: This program was tested and run on Ubuntu 18.04 and Mac OS 10.13. The C++ and Java code require recent compilers, and may not work on older systems.

## Dependencies

On Linux, the dependencies can be installed using the system package manager. Here we give instructions for Ubuntu, but other distributions will be similar.

```
$ sudo apt install build-essential git openjdk-11-jdk libgmp-dev libmpfr-dev libboost-all-dev libtbb-dev libjemalloc-dev
```

On a Mac, one first needs to install the Command Line Developer Tools, which contain a C++ compiler and various other utilities. In the terminal, run `xcode-select --install` and select the Install button. Next, we need to install the [Homebrew](http://brew.sh) package manager. Once you have followed the installation instructions on the website, the dependencies can be installed as follows.

```
$ brew cask install java
$ brew install git gmp mpfr boost tbb jemalloc
```

## Compiling

Next, we need to clone the repository and compile the code.

```
$ git clone https://bitbucket.org/gtokarsky/billiardviewersm.git
$ cd billiardviewersm
$ ./gradlew run
```

This will clone the repository using git, download the Java dependencies, compile the C++ and Java code, and run the GUI.

Alternatively, the C++ cover verifier can be run independently of the Java program. This can be done using the meson build system.

```
$ sudo apt install meson # Ubuntu
$ brew install meson     # Mac
```

Then, to setup meson

```
$ cd billiardviewersm
$ meson meson
$ ninja -C meson cover
$ meson/cover coversfolder/105cover     # check the 105cover, for example
```

